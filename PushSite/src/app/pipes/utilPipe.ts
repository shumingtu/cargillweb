import { Pipe, PipeTransform } from '@angular/core';
import { Constant } from '../../app/app.constant';
import * as moment from 'moment';
import * as _ from "lodash";

@Pipe({ name: 'dateFilter' })
export class DatePipe implements PipeTransform {
  transform(value: any, format: string): string {
    return value ? moment(value).format(format) : '';
  }
}