import { Observable } from 'rxjs/Rx';
import { Component, OnInit, Inject } from '@angular/core';
import { Validators, ValidatorFn, FormBuilder, FormGroup, FormControl, AbstractControl, RadioControlValueAccessor } from '@angular/forms';

import { GridDataResult } from '@progress/kendo-angular-grid';
import { State, process } from '@progress/kendo-data-query';

import { Constant } from './app.constant';

import { NotificationService } from './services/notificationService';
import { Notification } from './models/notification';

import * as moment from 'moment';
import swal from 'sweetalert2'

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
	public gridState: State = {
		sort: [{ field: 'Id', dir: 'desc' }],
		skip: 0,
		take: 10
	};
	public active = false;
	private editedRowIndex: number;
	public editForm: FormGroup;
	public isNew: boolean;
	public notifications: Notification[];
	public ScheduleTime: Date = new Date();
	public comfirm: Boolean = false;
	public files;

	constructor(private notificationService: NotificationService) { }

	public ngOnInit(): void {
		console.log("ngOnInit");
		this.editForm = new FormGroup({
			'Id': new FormControl(''),
			'Title': new FormControl(''),
			'Message': new FormControl('', Validators.required),
			'ExcelFile': new FormControl(),
			'HiddenExcelFile': new FormControl(''),
			'ScheduleTime': new FormControl(new Date()),
			'Type': new FormControl(''),
			'Range': new FormControl('0'),
			'Way': new FormControl('0')
		});
		this.getNotifications();
	}

	getNotifications(): void {
		this.notificationService.getNotifications()
			.subscribe(notifications => this.notifications = notifications);
	}

	//kendo add event
	addHandler() {
		this.editForm = new FormGroup({
			'Id': new FormControl(''),
			'Title': new FormControl('', Validators.required),
			'Message': new FormControl('', Validators.required),
			'ExcelFile': new FormControl(),
			'HiddenExcelFile': new FormControl(''),
			'ScheduleTime': new FormControl(new Date()),
			'Type': new FormControl(''),
			'Range': new FormControl('0', Validators.required),
			'Way': new FormControl('0', Validators.required)
		});
		//flag
		this.isNew = true;
		this.active = true;
	}
	//kendo edit event
	editHandler({ dataItem }) {
		this.isNew = false;
		this.editForm.reset(dataItem);
		this.editForm.get("Range").setValue("0");
		this.editForm.get("Way").setValue("0");
		this.active = true;
	}

	removeHandler({ dataItem }) {
		console.log(dataItem);
		this.notificationService.deleteNotification(dataItem)
			.subscribe(() => this.getNotifications());
	}

	onFileChange(event) {
		if (event.target.files && event.target.files.length >= 1) {
			this.files = event.target.files;
			console.log(this.files[0].name);
			this.editForm.get('hiddenExcelFile').setValue(this.files[0].name);
		}
	}

	onStateChange(state: State) {
		this.gridState = state;
	}

	private closeForm(): void {
		this.active = false;
	}

	public onSave(e): void {
		if (this.isNew) {
			this.comfirm = true;
		} else {
			this.notificationService.pushNotification(this.editForm.value, this.isNew)
				.subscribe((value) => {
					this.getNotifications();
					this.active = false;
				});
		}
	}

	public onCancel(e): void {
		this.active = false;
	}

	public onComfirmCancel(e): void {
		e.preventDefault();
		this.closeComfirmForm();
	}
	
	private closeComfirmForm(): void {
		this.comfirm = false;
	}

	public onComfirmSave(e): void {
		e.preventDefault();
		this.editForm.get('Type').setValue(parseInt((this.editForm.value['Range'] + this.editForm.value['Way']), 2));
		this.editForm.value['excelFile'] = this.files;
		console.log(this.editForm.value);
		if (this.editForm.value['Way'] === Constant.PushType.IMMEDIATE) {
			delete this.editForm.value['ScheduleTime'];
		}
		this.notificationService.pushNotification(this.editForm.value, this.isNew)
			.subscribe((value) => {
				if (value.Error) {
					swal({
						title: "推播失敗",
						type: "error",
					}).then((value) => {
						this.getNotifications();
					});
				} else {
					swal({
						title: "推播成功",
						type: "success",
					}).then((value) => {
						this.getNotifications();
					});
				}
			});
		this.active = false;
		this.comfirm = false;
	}
}
