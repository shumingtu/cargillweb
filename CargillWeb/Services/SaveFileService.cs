﻿using CargillWeb.Models.DBModels;
using CargillWeb.Models.NotificationModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;

namespace CargillWeb.Services
{
    public class SaveFileService
    {
        public static UploadPushFileResponse UploadPushFile(MultipartFormDataStreamProvider provider)
        {
            cargillEntities db = null;
            UploadPushFileResponse uploadPushFileResponse = new UploadPushFileResponse();
            var path = "";
            List<string> userIds = new List<string>();
            try
            {
                foreach (MultipartFileData file in provider.FileData)
                {
                    string fileName = file.Headers.ContentDisposition.FileName;
                    string newFileName = Path.GetFileName(file.LocalFileName);
                    string fullFilePath;
                    if (fileName.StartsWith("\"") && fileName.EndsWith("\""))
                    {
                        fileName = fileName.Trim('"');
                    }
                    if (fileName.Contains(@"/") || fileName.Contains(@"\"))
                    {
                        fileName = Path.GetFileName(fileName);
                    }
                    fullFilePath = ConfigurationManager.AppSettings["SavePushTempFilePath"] + newFileName;

                    using (var sr = new StreamReader(fullFilePath))
                    {
                        while (!sr.EndOfStream)
                        {
                            userIds.Add(sr.ReadLine().Split(',')[0]);
                        }
                    }

                    db = new cargillEntities();
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                    {
                        var device = (from item in db.deviceinfo
                                      where userIds.Contains(item.UserId)
                                      select item).OrderByDescending(x => x.LastTime).FirstOrDefault();

                        StringBuilder builder = new StringBuilder();
                        builder.AppendLine(device.DeviceToken);

                        path = ConfigurationManager.AppSettings["SavePushFilePath"] + newFileName;
                        File.WriteAllText(path, builder.ToString());
                        scope.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null) db.Dispose();
            }

            uploadPushFileResponse.userIds = userIds;
            uploadPushFileResponse.path = path;
            return uploadPushFileResponse;
        }
        public async static Task<string> SaveOrderMessage(List<string> userIdList)
        {
            cargillEntities db = null;
            var path = "";
            try
            {
                db = new cargillEntities();
                #region 取deviceToken 並存入csv
                StringBuilder userClause = new StringBuilder();
                userIdList = userIdList.Distinct<string>().ToList();
                for (var i = 0; i < userIdList.Count; i++)
                {
                    var userId = userIdList[i];

                    userClause.Append("\'" + userId + "\'");

                    if (i != userIdList.Count - 1)
                    {
                        userClause.Append(" , ");
                    }
                }
                
                string sql = "select UserId, DeviceToken from deviceinfo where UserId in( " + userClause + " ) ORDER BY LastTime DESC";
                var userDeviceModel = await db.Database.SqlQuery<UserDeviceModel>(sql).ToListAsync();

                List<string> deviceTokens = new List<string>();
                List<string> userIds = new List<string>();

                foreach (var item in userDeviceModel)
                {
                    if (!userIds.Contains(item.userId))
                    {
                        deviceTokens.Add(item.deviceToken);
                        userIds.Add(item.userId);
                    }
                }

                StringBuilder builder = new StringBuilder();
                foreach (var deviceToken in deviceTokens)
                {
                    builder.AppendLine(deviceToken);
                }

                path = ConfigurationManager.AppSettings["SavePushFilePath"] + UtilService.getOrderNo() + ".csv";
                File.WriteAllText(path, builder.ToString());

                //using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                //{
                //    var devices = (from item in db.deviceinfo
                //                  where userIds.Contains(item.UserId)
                //                  select item).OrderByDescending(x => x.LastTime).ToArray();

                //    StringBuilder builder = new StringBuilder();
                //    foreach (var device in devices)
                //    {
                //        builder.AppendLine(device.DeviceToken);
                //    }
                    
                //    path = ConfigurationManager.AppSettings["SavePushFilePath"] + UtilService.getOrderNo() + ".csv";

                //    File.WriteAllText(path, builder.ToString());
                //    scope.Complete();
                //}
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null) db.Dispose();
            }
            return path;
        }
        public static string FileNameFilter(string fileName)
        {
            fileName = fileName.Replace("\"", string.Empty);
            var specialWords = ConfigurationManager.AppSettings["SpecialWords"].Split(',');
            foreach (var specialWord in specialWords)
            {
                fileName = fileName.Replace(specialWord, string.Empty);
            }
            return fileName;
        }
    }
    public class CustomMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
    {
        public string path { get; set; }
        public CustomMultipartFormDataStreamProvider(string path) : base(path)
        {
            this.path = path;
        }

        public override string GetLocalFileName(HttpContentHeaders headers)
        {
            string fileName = SaveFileService.FileNameFilter(headers.ContentDisposition.Name);
            string pathFile = this.path + fileName;
            if (File.Exists(pathFile))
            {
                int index = fileName.LastIndexOf(".");
                fileName = fileName.Insert(index, UtilService.getOrderNo());
            }
            return fileName;
        }
    }
}