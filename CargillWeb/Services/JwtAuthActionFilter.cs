﻿using CargillWeb.Models.DBModels;
using Jose;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace CargillWeb.Services
{
    public class JwtAuthActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            cargillEntities db = null;
            var secret = "cargillApp";

            if (actionContext.Request.Headers.Authorization == null || actionContext.Request.Headers.Authorization.Scheme != "Bearer")
            {
                setErrorResponse(actionContext, "驗證錯誤");
            }
            else
            {
                try
                {
                    db = new cargillEntities();

                    var jwtObject = Jose.JWT.Decode<Dictionary<string, dynamic>>(
                        actionContext.Request.Headers.Authorization.Parameter,
                        Encoding.UTF8.GetBytes(secret),
                        JwsAlgorithm.HS256);
                    var userId = "";
                    userId = jwtObject["userId"];
                    var userType = "";
                    userType = jwtObject["userType"];
                    var lastLoginTime = "";
                    lastLoginTime = jwtObject["lastLoginTime"];

                    var userData = (from user in db.app_user
                                         where user.unicode == userId
                                         && user.type == userType
                                         select user).FirstOrDefault();

                    if (userData != null)
                    {
                        /*var tempStr = ((DateTime)userData.last_login_time).ToString("yyyy-MM-dd HH:mm:ss");
                        if (lastLoginTime == tempStr)
                        {
                            // token驗證成功
                            //setErrorResponse(actionContext, "userId:" + userId + "userType:" + userType + "lastLoginTime:" + lastLoginTime + "tempStr:" + tempStr + "一樣");
                        }
                        else
                        {
                            setErrorResponse(actionContext, "userId:" + userId + "userType:" + userType + "lastLoginTime:" + lastLoginTime + "tempStr:" + tempStr + "不一樣");
                        }*/
                    }
                    else
                    {
                        setErrorResponse(actionContext, "userId:" + userId + "userType:" + userType + "lastLoginTime:" + lastLoginTime + "查無資料");
                    }

                    //setErrorResponse(actionContext,"str:" + str + " userId:" + jwtObject["userId"] + " userType:" + jwtObject["userType"] + " lastLoginTime:" + jwtObject["lastLoginTime"]);
                }
                catch (Exception ex)
                {
                    setErrorResponse(actionContext, ex.Message);
                }
            }
    
            base.OnActionExecuting(actionContext);
        }

        private static void setErrorResponse(HttpActionContext actionContext, string message)
        {
            var response = actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, message);
            actionContext.Response = response;
        }
    }
}