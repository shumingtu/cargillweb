﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Services
{
    public class UtilService
    {
        public static string getOrderNo()
        {
            return DateTime.Now.ToString("yyyyMMddHHmmssff") + "_" + Guid.NewGuid().ToString().Split('-')[0];
        }
    }
}