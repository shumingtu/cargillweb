﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.OrderModel.RequestBody
{
    public class AddOrderReqBody
    {
        public List<foodObj> foods { get; set; }
        public string customerId { get; set; }
        public string customerName { get; set; }
        public string orderPerson { get; set; }
        public string tmcode { get; set; }
        public string customerPhone { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string note { get; set; }
        public string location { get; set; }
    }

    public class foodObj
    {
        public string code { get; set; }
        public string name { get; set; }
        public string unit { get; set; }
        public float count { get; set; }
        public string spec { get; set; }
        public float weight { get; set; }
        public string phase { get; set; }
        public string price { get; set; }
        public detailSP detailSP { get; set; }
        public string vehicleType { get; set; }
        public string vehicleName { get; set; }
    }

    public class detailSP
    {
        public string id { get; set; }
        public string value { get; set; }
        public string createDate { get; set; }
        public List<int> symptom { get; set; }
        public string deliveryDate { get; set; }
        public string productSpec { get; set; }
        public string productLineNote { get; set; }
        public string productNote { get; set; }
        public string noteA { get; set; }
        public string noteB { get; set; }
        public string noteC { get; set; }
        public List<drugList> drugCompanyList { get; set; }
        public List<drugList> drugSelfList { get; set; }
        public float totalPrice { get; set; }
        public float codeWeight { get; set; }
        public float spPrice { get; set; }
        public List<string> modifyLogs { get; set; }

    }

    public class drugList
    {
        public string code { get; set; }
        public string name { get; set; }
        public float count1kg { get; set; }
        public float count2kg { get; set; }
        public string recommend { get; set; }
        public float totalCount { get; set; }
        public string days { get; set; }
        public string note { get; set; }
        public float price { get; set; }
    }
}