﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.OrderModel.RequestBody
{
    public class SendOrderEndMessageReqBody
    {
        public string orderNo { get; set; }
        public string customerId { get; set; }
        public string poCode { get; set; }
        public string type { get; set; }
    }
}