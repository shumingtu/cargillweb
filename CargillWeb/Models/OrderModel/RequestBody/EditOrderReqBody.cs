﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.OrderModel.RequestBody
{
    public class EditOrderReqBody : AddOrderReqBody
    {
        public string orderNo { get; set; }
        public Nullable<DateTime> modify_time { get; set; }
    }
}