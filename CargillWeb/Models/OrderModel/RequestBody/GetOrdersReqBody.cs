﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.OrderModel.RequestBody
{
    public class GetOrdersReqBody
    {
        public string customerId { get; set; }
        public string timeFilter { get; set; }
    }
}