﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.OrderModel.RequestBody
{
    public class DeleteOrderReqBody
    {
        public string orderNo { get; set; }
        public string customerId { get; set; }
        public string orderPerson { get; set; }
    }
}