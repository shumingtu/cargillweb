﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.OrderModel.ResponseBody
{
    public class EditOrderResBody : GenericResBody
    {
        public string details { get; set; }
    }
}