﻿using CargillWeb.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.OrderModel.ResponseBody
{
    public class GetOrdersResBody : GenericResBody
    {
        public List<customer_order> orders { get; set; }
    }
}