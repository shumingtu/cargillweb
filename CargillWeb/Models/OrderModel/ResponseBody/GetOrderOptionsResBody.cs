﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.OrderModel.ResponseBody
{
    public class GetOrderOptionsResBody : GenericResBody
    {
        public GetOrderOptionsResBody()
        {
            this.orderOptions = new OrderOptions();
        }
        public OrderOptions orderOptions { get; set; }

        public CanDeliveredTime canDeliveredTime { get; set; }
    }

    public class OrderOptions
    {
        public OrderOptions()
        {
            this.shipToOptions = new List<ShipToOptions>();
            this.foodOptions = new List<FoodOptions>();
            this.drugOptions = new List<DrugOptions>();
        }
        public List<ShipToOptions> shipToOptions { get; set; }
        public List<FoodOptions> foodOptions { get; set; }
        public List<DrugOptions> drugOptions { get; set; }
    }

    public class ShipToOptions
    {
        public string address { get; set; }
    }

    public class FoodOptions
    {
        public string foodId { get; set; }
        public string foodName { get; set; }
    }

    public class DrugOptions
    {
        public string drugId { get; set; }
        public string drugName { get; set; }
        public string max_amount { get; set; }
        public string recommended_amount { get; set; }
        public string type { get; set; }
    }
}