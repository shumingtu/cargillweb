﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.OrderModel.ResponseBody
{
    public class CanDeliveredTime
    {
        public string canDeliveredDate { get; set; }
        public List<string> canDeliveredyHours { get; set; }
    }
}