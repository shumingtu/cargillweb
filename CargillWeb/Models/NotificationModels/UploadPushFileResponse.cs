﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.NotificationModels
{
    public class UploadPushFileResponse
    {
        public List<string> userIds { get; set; }
        public string path { get; set; }
    }
}