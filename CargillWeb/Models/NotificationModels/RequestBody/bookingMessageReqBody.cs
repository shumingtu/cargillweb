﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.NotificationModels.RequestBody
{
    public class BookingMessageReqBody
    {
        public List<string> userIds { set; get; }
        public string title { set; get; }
        public string message { set; get; }
    }
}