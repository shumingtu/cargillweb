﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.MessageModel
{
    public class OrderMessageModel
    {
        public string message { get; set; }
        public string title { get; set; }
        public DateTime createTime { get; set; }
        public string tmcode { get; set; }
    }
}