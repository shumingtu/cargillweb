﻿using CargillWeb.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.MessageModel.ResponseBody
{
    public class GetMessagesResBody : GenericResBody
    {
        public GetMessagesResBody()
        {
            this.messages = new List<message>();
        }
        public List<message> messages { get; set; }
    }

    public class message
    {
        public string messageId { get; set; }
        public string content { get; set; }
        public string title { get; set; }
        public DateTime createTime { get; set; }
    }
}