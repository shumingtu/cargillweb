﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.MessageModel.RequestBody
{
    public class GetMessagesReqBody
    {
        public string userId { get; set; }
        public string userType { get; set; }
    }
}