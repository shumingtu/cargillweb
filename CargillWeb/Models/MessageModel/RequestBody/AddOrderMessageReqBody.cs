﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.MessageModel.RequestBody
{
    public class AddOrderMessageReqBody
    {
        public string tmcode { get; set; }
        public string customerId { get; set; }
    }
}