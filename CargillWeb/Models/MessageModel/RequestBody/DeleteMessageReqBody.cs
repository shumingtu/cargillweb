﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.MessageModel.RequestBody
{
    public class DeleteMessageReqBody
    {
        public string userId { get; set; }
        public string userType { get; set; }
        public List<string> messageIds { get; set; }
    }
}