﻿using CargillWeb.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.UserModel.ResponseBody
{
    public class GetSubCustomersResBody : GenericResBody
    {
        public GetSubCustomersResBody()
        {
            this.subCustomers = new List<subCustomerData>();
        }

        public List<subCustomerData> subCustomers { get; set; }
    }

    public class subCustomerData
    {
        public string id { get; set; }
        public string name { get; set; }
        public string mcId { get; set; }
        public string mcName { get; set; }
        public string tmcode { get; set; }
        public string phone { get; set; }
        public string shipTo { get; set; }
    }
}