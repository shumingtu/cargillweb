﻿using CargillWeb.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.UserModel.ResponseBody
{
    public class AddAppUserResBody : GenericResBody
    {
        public List<app_user> data { get; set; }
    }
}