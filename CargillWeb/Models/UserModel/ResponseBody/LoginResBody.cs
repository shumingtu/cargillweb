﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.UserModel.ResponseBody
{
    public class LoginResBody : GenericResBody
    {
        public string name { get; set; }
        public string type { get; set; }
        public string token { get; set; }
    }
}