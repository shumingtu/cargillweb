﻿using CargillWeb.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.UserModel.ResponseBody
{
    public class GetDrugInventoryResBody : GenericResBody
    {
        public List<drug_inventory> inventoryList { get; set; }
    }
}