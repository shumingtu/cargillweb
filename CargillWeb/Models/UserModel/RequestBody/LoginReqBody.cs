﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.UserModel.RequestBody
{
    public class LoginReqBody
    {
        public string userId { get; set; }
        public string password { get; set; }
    }
}