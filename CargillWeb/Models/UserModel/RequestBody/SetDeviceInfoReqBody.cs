﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.UserModel.RequestBody
{
    public class SetDeviceInfoReqBody
    {
        public string deviceId { set; get; }
        public string userId { set; get; }
        public string deviceType { set; get; }
        public string deviceModel { set; get; }
        public string deviceResolution { set; get; }
        public string deviceVersion { set; get; }
        public string deviceManufacturer { set; get; }
        public string deviceToken { set; get; }
        public string appVersion { set; get; }
    }
}