﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.UserModel.RequestBody
{
    public class AddAppUserReqBody
    {
        public string phone { get; set; }
        public string type { get; set; }
        public string unicode { get; set; }
        public string user_name { get; set; }
    }
}