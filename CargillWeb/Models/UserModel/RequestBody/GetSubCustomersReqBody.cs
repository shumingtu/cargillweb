﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.UserModel.RequestBody
{
    public class GetSubCustomersReqBody
    {
        public string userId { get; set; }
        public string type { get; set; }
    }
}