//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace CargillWeb.Models.DBModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class app_user
    {
        public long id { get; set; }
        public System.DateTime create_time { get; set; }
        public Nullable<System.DateTime> modify_time { get; set; }
        public string can_act { get; set; }
        public string email { get; set; }
        public Nullable<System.DateTime> last_login_time { get; set; }
        public string password { get; set; }
        public string phone { get; set; }
        public string type { get; set; }
        public string unicode { get; set; }
        public string user_name { get; set; }
        public string verify_code { get; set; }
        public Nullable<int> verify_counts { get; set; }
        public Nullable<System.DateTime> verify_lasttime { get; set; }
        public Nullable<System.DateTime> verify_createtime { get; set; }
        public string verify_result { get; set; }
        public Nullable<int> verify_send_counts { get; set; }
    }
}
