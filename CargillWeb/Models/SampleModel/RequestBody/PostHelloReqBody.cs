﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.SampleModel.RequestBody
{
    public class PostHelloReqBody
    {
        public string name { get; set; }
        public int age { get; set; }
    }
}