﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.SampleModel.ResponseBody
{
    public class PostHelloResBody : GenericResBody
    {
        public string result { get; set; }

    }
}