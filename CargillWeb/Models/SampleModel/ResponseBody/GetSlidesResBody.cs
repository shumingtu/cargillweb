﻿using CargillWeb.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.SampleModel.ResponseBody
{
    public class GetSlidesResBody : GenericResBody
    {
        public GetSlidesResBody()
        {
            this.slides = new List<slidesData>();
        }

        public int? time { get; set; }
        public List<slidesData> slides { get; set; }
    }

    public class slidesData
    {
        public string file { get; set; }
        public string link { get; set; }
    }
}