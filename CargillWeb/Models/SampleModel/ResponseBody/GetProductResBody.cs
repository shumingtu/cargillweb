﻿using CargillWeb.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargillWeb.Models.SampleModel.ResponseBody
{
    public class GetProductResBody : GenericResBody
    {
        public List<product> products { get; set; }
    }
}