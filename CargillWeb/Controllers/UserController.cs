﻿using CargillWeb.Models.DBModels;
using CargillWeb.Models.UserModel.RequestBody;
using CargillWeb.Models.UserModel.ResponseBody;
using CargillWeb.Services;
using Jose;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CargillWeb.Controllers
{
    [EnableCors("*", "*", "*")]
    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {
        [DeflateCompression]
        [HttpPost]
        [Route("login")]
        public async Task<object> login(LoginReqBody reqBody)
        {
            cargillEntities db = null;
            LoginResBody resBody = null;
            var secret = "cargillApp";

            try
            {
                db = new cargillEntities();
                resBody = new LoginResBody();

                if (reqBody.userId.Trim() == "" || reqBody.password.Trim() == "")
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                    {
                        Content = new StringContent("帳號密碼不能空白")
                    };
                    throw new HttpResponseException(resp);
                }

                SHA256 sha256 = new SHA256CryptoServiceProvider();
                string salt = "123cargill";
                string result = Convert.ToBase64String(sha256.ComputeHash(Encoding.Default.GetBytes(reqBody.password + salt)));

                var userData = await (from user in db.app_user
                                        where user.unicode == reqBody.userId
                                        && user.password == result
                                        select user).FirstOrDefaultAsync();

                if (userData != null)
                {
                    //檢查開通狀態
                    if (userData.verify_result != "T")
                    {
                        throw new Exception("帳號尚未開通");
                    }

                    //取得帳號資料
                    resBody.name = userData.user_name;
                    resBody.type = userData.type;

                    //寫入最後登入時間
                    var timeStr = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    DateTime lastLoginTime = DateTime.Parse(timeStr);

                    userData.last_login_time = lastLoginTime;
                    db.Entry(userData).State = EntityState.Modified;
                    await db.SaveChangesAsync();

                    var payload = new Dictionary<string, dynamic>()
                    {
                        { "userId", reqBody.userId },
                        { "userType", userData.type },
                        { "lastLoginTime", timeStr }
                    };

                    resBody.token = JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS256);
                    
                }
                else
                {
                    //throw new Exception("帳號密碼錯誤");
                    resBody.error = "帳號密碼錯誤";
                }
            }
            catch (Exception ex)
            {
                //resBody.error = ex.ToString();
                //resBody.error = ex.Message;
                resBody.error = "系統忙碌中";
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return resBody;
        }

        [DeflateCompression]
        [HttpPost]
        [Route("getSubCustomers")]
        [JwtAuthActionFilter]
        public async Task<GetSubCustomersResBody> getSubCustomers(GetSubCustomersReqBody reqBody)
        {
            cargillEntities db = null;
            GetSubCustomersResBody resBody = null;

            try
            {
                db = new cargillEntities();
                resBody = new GetSubCustomersResBody();

                if (reqBody.type == "MC") //主帳號
                {
                    var subCustomers = await (from customer in db.customer
                                              where customer.customer_main_id == reqBody.userId
                                              select new
                                              {
                                                  customer.customer_sub_id,
                                                  customer.customer_sub_name,
                                                  customer.customer_main_id,
                                                  customer.customer_main_name,
                                                  customer.tmcode,
                                                  customer.phone,
                                                  customer.address_ship_to,
                                              }).Distinct().OrderBy(x => x.customer_sub_id).ToListAsync();
                    if (subCustomers != null)
                    {
                        foreach (var subCustomer in subCustomers)
                        {
                            resBody.subCustomers.Add(new subCustomerData()
                            {
                                id = subCustomer.customer_sub_id,
                                name = subCustomer.customer_sub_name,
                                mcId = subCustomer.customer_main_id,
                                mcName = subCustomer.customer_main_name,
                                tmcode = subCustomer.tmcode,
                                phone = subCustomer.phone,
                                shipTo = subCustomer.address_ship_to,
                            });
                        }
                    }
                }
                else if (reqBody.type == "SC") //子帳號
                {
                    var subCustomers = await (from customer in db.customer
                                              where customer.customer_sub_id == reqBody.userId
                                              select new
                                              {
                                                  customer.customer_sub_id,
                                                  customer.customer_sub_name,
                                                  customer.customer_main_id,
                                                  customer.customer_main_name,
                                                  customer.tmcode,
                                                  customer.phone,
                                                  customer.address_ship_to,
                                              }).FirstOrDefaultAsync();

                    if (subCustomers != null)
                    {
                        resBody.subCustomers.Add(new subCustomerData()
                        {
                            id = subCustomers.customer_sub_id,
                            name = subCustomers.customer_sub_name,
                            mcId = subCustomers.customer_main_id,
                            mcName = subCustomers.customer_main_name,
                            tmcode = subCustomers.tmcode,
                            phone = subCustomers.phone,
                            shipTo = subCustomers.address_ship_to,
                        });
                    }
                }
                else if (reqBody.type == "S") //業務
                {
                    var subCustomers = await (from customer in db.customer
                                              where customer.tmcode == reqBody.userId
                                              select new
                                              {
                                                  customer.customer_sub_id,
                                                  customer.customer_sub_name,
                                                  customer.customer_main_id,
                                                  customer.customer_main_name,
                                                  customer.tmcode,
                                                  customer.phone,
                                                  customer.address_ship_to,
                                              }).Distinct().OrderBy(x => x.customer_sub_id).ToListAsync();

                    if (subCustomers != null)
                    {
                        foreach (var subCustomer in subCustomers)
                        {
                            resBody.subCustomers.Add(new subCustomerData()
                            {
                                id = subCustomer.customer_sub_id,
                                name = subCustomer.customer_sub_name,
                                mcId = subCustomer.customer_main_id,
                                mcName = subCustomer.customer_main_name,
                                tmcode = subCustomer.tmcode,
                                phone = subCustomer.phone,
                                shipTo = subCustomer.address_ship_to,
                            });
                        }
                    }
                }
                else
                {
                    resBody.error = "帳號類型不合";
                }

                if (resBody.subCustomers.Count == 0)
                {
                    resBody.error = "查無資料";
                }

            }
            catch (Exception ex)
            {
                //resBody.error = ex.ToString();
                resBody.error = "系統忙碌中";
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return resBody;
        }

        [DeflateCompression]
        [HttpPost]
        [Route("setPassword")]
        public async Task<SetPasswordResBody> setPassword(SetPasswordReqBody reqBody)
        {
            cargillEntities db = null;
            SetPasswordResBody resBody = null;

            try
            {
                db = new cargillEntities();
                resBody = new SetPasswordResBody();

                if (reqBody.userId.Trim() == "" || reqBody.password.Trim() == "" || reqBody.otp.Length != 4)
                {
                    resBody.error = "欄位格式錯誤";
                    throw new Exception();
                }

                var userData = await (from user in db.app_user
                                      where user.unicode == reqBody.userId
                                      select user).FirstOrDefaultAsync();

                if (userData != null)
                {
                    //檢查帳號是否可開通
                    if (userData.can_act == "F")
                    {
                        resBody.error = "此帳號不可開通";
                        throw new Exception();
                    }

                    //檢查驗證碼
                    if (userData.verify_code == reqBody.otp)
                    {
                        userData.verify_lasttime = DateTime.Now;
                        userData.verify_counts = 0;

                        //檢查驗證碼時間
                        /*
                        DateTime nowTime = DateTime.Now.AddMinutes(-5);
                        DateTime createTime = userData.verify_createtime ?? DateTime.Now;

                        if (DateTime.Compare(nowTime, createTime) < 0)
                        {
                            userData.verify_lasttime = DateTime.Now;
                            userData.verify_counts = 0;
                        }
                        else
                        {
                            throw new Exception("驗證碼逾時，請洽客服");
                        }
                        */
                    }
                    else
                    {
                        if (userData.verify_counts < 2) //前兩次失敗
                        {
                            userData.verify_lasttime = DateTime.Now;
                            userData.verify_counts++;

                            db.Entry(userData).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                            resBody.error = "驗證碼錯誤";
                            throw new Exception();
                        }
                        else //第三次失敗鎖住帳號
                        {
                            userData.verify_lasttime = DateTime.Now;
                            userData.verify_counts++;

                            userData.can_act = "F";
                            userData.verify_result = "F";

                            db.Entry(userData).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                            resBody.error = "帳號已鎖住，請洽客服";
                            throw new Exception();
                        }
                    }


                    //設定密碼
                    SHA256 sha256 = new SHA256CryptoServiceProvider();
                    string salt = "123cargill";
                    string result = Convert.ToBase64String(sha256.ComputeHash(Encoding.Default.GetBytes(reqBody.password + salt)));

                    userData.password = result;
                    userData.verify_result = "T";

                    db.Entry(userData).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }
                else
                {
                    resBody.error = "查無帳號";
                    throw new Exception();
                }

            }
            catch (Exception ex)
            {
                //resBody.error = ex.ToString();
                //resBody.error = ex.Message;
                if (string.IsNullOrEmpty(resBody.error))
                {
                    resBody.error = "系統忙碌中";
                }
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return resBody;
        }

        [DeflateCompression]
        [HttpPost]
        [Route("setDeviceInfo")]
        public async Task<SetDeviceInfoResBody> setDeviceInfo(SetDeviceInfoReqBody reqBody)
        {
            var resBody = new SetDeviceInfoResBody();
            cargillEntities db = null;

            try
            {
                db = new cargillEntities();

                var deviceInfo = await (from item in db.deviceinfo
                                        where item.Id == reqBody.deviceId
                                        select item).FirstOrDefaultAsync();

                if (deviceInfo == null)
                {
                    deviceInfo = new deviceinfo()
                    {
                        Id = reqBody.deviceId,
                        UserId = reqBody.userId,
                        DeviceType = reqBody.deviceType,
                        DeviceModel = reqBody.deviceModel,
                        DeviceResolution = reqBody.deviceResolution,
                        DeviceVersion = reqBody.deviceVersion,
                        DeviceManufacturer = reqBody.deviceManufacturer,
                        DeviceToken = reqBody.deviceToken,
                        AppVersion = reqBody.appVersion,
                        LastTime = DateTime.Now
                    };

                    db.deviceinfo.Add(deviceInfo);
                }
                else
                {
                    deviceInfo.UserId = reqBody.userId;
                    deviceInfo.DeviceType = reqBody.deviceType;
                    deviceInfo.DeviceModel = reqBody.deviceModel;
                    deviceInfo.DeviceResolution = reqBody.deviceResolution;
                    deviceInfo.DeviceVersion = reqBody.deviceVersion;
                    deviceInfo.DeviceManufacturer = reqBody.deviceManufacturer;
                    deviceInfo.DeviceToken = reqBody.deviceToken;
                    deviceInfo.AppVersion = reqBody.appVersion;
                    deviceInfo.LastTime = DateTime.Now;

                    db.Entry(deviceInfo).State = EntityState.Modified;
                }

                await db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                //resBody.error = ex.ToString();
                resBody.error = "系統忙碌中";
            }
            finally
            {
                if (db != null) db.Dispose();
            }

            return resBody;
        }

        [DeflateCompression]
        [HttpGet]
        [Route("checkAppVersion")]
        public async Task<bool> checkAppVersion(string deviceType, string version)
        {
            cargillEntities db = null;
            var isValid = false;

            try
            {
                db = new cargillEntities();

                var type = (deviceType.ToLower() == "android") ? "App_Android" : "App_iOS";

                var isExist = await (from item in db.app_version
                                    where item.Type == type && item.Number == version && item.IsActive == 1
                                    select new
                                    {
                                        item.Id
                                    }).FirstOrDefaultAsync();

                if (isExist != null)
                {
                    isValid = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null) db.Dispose();
            }

            return isValid;
        }

        [DeflateCompression]
        [HttpPost]
        [Route("getDrugInventories")]
        [JwtAuthActionFilter]
        public async Task<GetDrugInventoryResBody> getDrugInventories(GetDrugInventoryReqBody reqBody)
        {
            cargillEntities db = null;
            GetDrugInventoryResBody resBody = null;

            try
            {
                db = new cargillEntities();
                resBody = new GetDrugInventoryResBody();
             
                var customerlist = await db.customer.Where(a => a.customer_sub_id == reqBody.customerSubId).ToListAsync();
                var drugIventories = new List<drug_inventory>();
                if (customerlist.Count > 0)
                {
                    string mainid = customerlist[0].customer_main_id.ToString();
                    //drug_inventory customer_sub_id資料改為customer_main_id
                    drugIventories = await db.drug_inventory.Where(a => a.customer_sub_id == mainid && a.status == "1")
                                                      .OrderByDescending(b => b.create_time).ToListAsync();
                }
                resBody.inventoryList = drugIventories;
            }
            catch (Exception ex)
            {
                //resBody.error = ex.ToString();
                resBody.error = "系統忙碌中";
            }
            finally
            {
                db.Dispose();
            }

            return resBody;
        }
    }
}
