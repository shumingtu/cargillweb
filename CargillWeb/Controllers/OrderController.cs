﻿using CargillWeb.Models.DBModels;
using CargillWeb.Models.OrderModel;
using CargillWeb.Models.OrderModel.RequestBody;
using CargillWeb.Models.OrderModel.ResponseBody;
using CargillWeb.Models.SampleModel.ResponseBody;
using CargillWeb.Services;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;

namespace CargillWeb.Controllers
{
    [EnableCors("*", "*", "*")]
    [RoutePrefix("api/order")]
    public class OrderController : ApiController
    {
        private static ILog log = LogManager.GetLogger(typeof(OrderController));

        [DeflateCompression]
        [HttpGet]
        [Route("getUUID")]
        public async Task<string> getUUID()
        {
            return UtilService.getOrderNo();
        }

        [DeflateCompression]
        [HttpPost]
        [Route("getOrders")]
        [JwtAuthActionFilter]
        public async Task<GetOrdersResBody> getOrders(GetOrdersReqBody reqBody)
        {
            cargillEntities db = null;
            GetOrdersResBody resBody = null;

            try
            {
                db = new cargillEntities();
                resBody = new GetOrdersResBody();

                var nowTime = DateTime.Now.AddMonths(-3);

                if (reqBody.timeFilter == "3d")
                {
                    nowTime = DateTime.Now.AddDays(-3);
                }

                var orders = await db.customer_order.Where(a => a.customer_sub_id == reqBody.customerId && DateTime.Compare(nowTime, a.create_time) < 0 && a.type != "DEL")
                                                    .OrderByDescending(b => b.create_time).ToListAsync();
                resBody.orders = orders;
            }
            catch (Exception ex)
            {
                //resBody.error = ex.ToString();
                resBody.error = "系統忙碌中";
            }
            finally
            {
                db.Dispose();
            }

            return resBody;
        }

        [DeflateCompression]
        [HttpPost]
        [Route("getOrderOptions")]
        [JwtAuthActionFilter]
        public async Task<GetOrderOptionsResBody> getOrderOptions(GetOrderOptionsReqBody reqBody)
        {
            cargillEntities db = null;
            GetOrderOptionsResBody resBody = null;

            try
            {
                db = new cargillEntities();
                resBody = new GetOrderOptionsResBody();
                List<string> foodIds = new List<string>();
                List<string> drugIds = new List<string>();
                string area_code;
                string area_type;

                var subCustomers = await (from customer in db.customer
                                          where customer.customer_sub_id == reqBody.customerId
                                          select new
                                          {
                                              customer.address_ship_to,
                                          }).ToListAsync();
                if (subCustomers != null)
                {
                    foreach (var subCustomer in subCustomers)
                    {
                        resBody.orderOptions.shipToOptions.Add(new ShipToOptions()
                        {
                            address = subCustomer.address_ship_to,
                        });
                    }
                }

                //只撈web訂單當選項
                var orders = await db.customer_order.Where(a => a.customer_sub_id == reqBody.customerId && a.type != "APP").OrderByDescending(x=>x.create_time).ToListAsync();

                foreach (var order in orders)
                {
                    // 1. parse 訂單details欄位，撈出歷史紀錄
                    WebOrderModel[] detailArray = JsonConvert.DeserializeObject<WebOrderModel[]>(order.details);

                    foreach (var detail in detailArray)
                    {
                        foodIds.Add(detail.code);

                        area_code = "";
                        area_type = "";
                        if (detail.detailSP != null && detail.detailSP.value != null)
                        {
                            switch (detail.detailSP.value)
                            {
                                case "台中/大肚(20)":
                                    area_code = "TWENTY";
                                    area_type = "TC";
                                    break;
                                case "高雄/威祥(40)":
                                    area_code = "FOURTY";
                                    area_type = "KS";
                                    break;
                                case "高雄/嘉泰(50)":
                                    area_code = "FIFTY";
                                    area_type = "KS";
                                    break;
                                case "高雄/東農(60)":
                                    area_code = "SIXTY";
                                    area_type = "KS";
                                    break;
                                default:
                                    area_code = "NULL";
                                    area_type = "KS";
                                    break;
                            }
                        }

                        if (detail.detailSP != null && detail.detailSP.drugCompanyList != null)
                        {
                            foreach (var companyDrug in detail.detailSP.drugCompanyList)
                            {
                                if (detail.detailSP.value.Contains("高雄"))
                                {
                                    //area_code = "NULL";
                                    drugIds.Add("NULL" + "_" + area_type + "_COMPANY_" + companyDrug.code);
                                }
                                else
                                {
                                    drugIds.Add(area_code + "_" + area_type + "_COMPANY_" + companyDrug.code);
                                }
                            }
                        }

                        if (detail.detailSP != null && detail.detailSP.drugSelfList != null)
                        {
                            foreach (var selfDrug in detail.detailSP.drugSelfList)
                            {
                                drugIds.Add(area_code + "_" + area_type + "_SELF_" + selfDrug.code);
                            }
                        }
                    }
                }

                // 2. 撈出飼料的詳細資料
                var foods = await db.product.Where(a => foodIds.Contains(a.code))
                                            .ToListAsync();

                foreach (var food in foods)
                {
                    resBody.orderOptions.foodOptions.Add(new FoodOptions()
                    {
                        foodId = food.code,
                        foodName = food.name,
                    });
                }


                // 3. 撈出特配的詳細資料
                var drugs = await db.producttpdrug.Where(a => drugIds.Contains(a.complex_code))
                                                        .ToListAsync();

                foreach (var drug in drugs)
                {
                    if(resBody.orderOptions.drugOptions.Where(x=> x.drugId == drug.code).FirstOrDefault() == null)
                    {
                        resBody.orderOptions.drugOptions.Add(new DrugOptions()
                        {
                            drugId = drug.code,
                            drugName = drug.name,
                            max_amount = drug.max_amount,
                            recommended_amount = drug.recommended_amount,
                            type = (drug.drug_type == "COMPANY") ? "COMPANY" : "SELF",
                        });
                    }
                }

                //可送達時間
                resBody.canDeliveredTime = getCanDeliveredTime();

            }
            catch (Exception ex)
            {
                //resBody.error = ex.ToString();
                resBody.error = "系統忙碌中";
            }
            finally
            {
                db.Dispose();
            }

            return resBody;
        }

        [DeflateCompression]
        [HttpPost]
        [Route("addOrder")]
        [JwtAuthActionFilter]
        public async Task<AddOrderResBody> addOrder(AddOrderReqBody reqBody)
        {
            cargillEntities db = null;
            AddOrderResBody resBody = null;

            try
            {
                var now = DateTime.Now;
                db = new cargillEntities();
                resBody = new AddOrderResBody();

                if (reqBody.location == null | reqBody.date == null | reqBody.time == null || reqBody.foods.Count == 0)
                {
                    //throw new Exception("訂單欄位有缺");
                    resBody.error = ("訂單欄位有缺");
                    return resBody;
                }

                var timeStr = "";
                switch (reqBody.time)
                {
                    case "上午":
                        timeStr = " 09:00";
                        break;
                    case "中午":
                        timeStr = " 12:00";
                        break;
                    case "下午":
                        timeStr = " 15:00";
                        break;
                    default:
                        timeStr = " " + reqBody.time;
                        break;
                }

                var products = db.product.ToList();
                var productdrugs = db.producttpdrug.ToList();

                //對每個特配單設定出貨時間
                var count = 1;
                foreach (var food in reqBody.foods)
                {
                    var product = products.Where(x => x.code == food.code).FirstOrDefault();
                    if (product != null)
                    {
                        food.phase = product.phase;

                        //包裝、散裝算Weight
                        /*
                        "weight"的邏輯為(意義上為總重量，單位是噸)
                        "unit": "散裝/x",為散裝時，其值就相等
                        "unit": "30kg/3",為包裝時，其值就是這unit / count兩個欄位相乘 ， App的包裝固定為30kg/3
                        */

                        if (food.unit.Contains("散裝"))
                        {
                            food.weight = food.count;
                            food.price = product.pricex.Value.ToString();
                        }
                        else
                        {
                            food.weight = 30 * food.count / 1000;
                            food.price = product.price30kg3.Value.ToString();
                        }
                    }
                    if (food.detailSP != null)
                    {
                        food.detailSP.deliveryDate = reqBody.date + timeStr;

                        //避免重複
                        food.detailSP.id = DateTime.Now.ToString("HHmmss") + count.ToString().PadLeft(3, '0');
                        count++;

                        food.detailSP.value = "特配單";

                        if (food.detailSP.drugCompanyList != null)
                        {
                            //塞初始資料
                            foreach (var drugCompany in food.detailSP.drugCompanyList)
                            {
                                var productdrug = productdrugs.Where(x => x.code == drugCompany.code).FirstOrDefault();
                                drugCompany.count2kg = drugCompany.count1kg * 2;
                                drugCompany.recommend = !string.IsNullOrEmpty(productdrug.recommended_amount) ? productdrug.recommended_amount : "/";

                                drugCompany.totalCount = food.weight * drugCompany.count1kg;

                                drugCompany.days = productdrug.drug_withdrawal;
                                drugCompany.note = productdrug.note;
                                drugCompany.price = 0;
                            }

                            foreach (var drugSelf in food.detailSP.drugSelfList)
                            {
                                //var productdrug = productdrugs.Where(x => x.code == drugSelf.code).FirstOrDefault();
                                var productdrug = productdrugs.Where(x => x.name == drugSelf.name).FirstOrDefault();
                                if(productdrug != null)
                                {
                                    drugSelf.count2kg = drugSelf.count1kg * 2;
                                    drugSelf.recommend = !string.IsNullOrEmpty(productdrug.recommended_amount) ? productdrug.recommended_amount : "/";

                                    drugSelf.totalCount = food.weight * drugSelf.count1kg;

                                    drugSelf.days = productdrug.drug_withdrawal;
                                    drugSelf.note = productdrug.note;
                                    drugSelf.price = 0;
                                }
                            }
                        }
                    }
                }

                var details = JsonConvert.SerializeObject(reqBody.foods);
                resBody.details = details;

                var order = new customer_order()
                {
                    details = details,
                    ship_to = reqBody.location,
                    create_time = DateTime.Now,
                    delivery_date = DateTime.Parse(reqBody.date + timeStr),
                    desc_app = reqBody.note,
                    customer_sub_id = reqBody.customerId,
                    customer_sub_name = reqBody.customerName,
                    customer_phone = reqBody.customerPhone,
                    order_person = reqBody.orderPerson,
                    tmcode = reqBody.tmcode,

                    type = "APP",
                    order_sys = "APP",
                    modify_logs = DateTime.Now.ToString("yyyyMMdd HH:mm") + " APP " + reqBody.orderPerson + " 新增,",
                    order_no = UtilService.getOrderNo(),

                };
                db.customer_order.Add(order);

                var mainCustomer = await (from customer in db.customer
                                          where customer.customer_sub_id == reqBody.customerId
                                          select customer).FirstOrDefaultAsync();
                // 訊息頁內文
                var messageOrderCustomer = "";
                var messageOrderDetail = "";

                messageOrderCustomer += reqBody.tmcode + "您好，客戶 " + mainCustomer.customer_sub_name + "(" + mainCustomer.customer_sub_id + ") 一筆新單\n";
                messageOrderCustomer += "子帳號:" + mainCustomer.customer_sub_name + "(" + mainCustomer.customer_sub_id + ")\n";
                messageOrderCustomer += "主帳號:" + mainCustomer.customer_main_name + "(" + mainCustomer.customer_main_id + ")\n\n";

                messageOrderDetail += "希望到貨日期：" + reqBody.date + " " + reqBody.time + "\n";
                messageOrderDetail += "送貨地點：" + reqBody.location + "\n\n";
                foreach (var food in reqBody.foods)
                {
                    messageOrderDetail += food.code + " - " + food.name + " - " + food.unit + " ( " + food.count + " )\n";
                    
                    if (food.detailSP != null)
                    {
                        foreach (var drug in food.detailSP.drugCompanyList)
                        {
                            messageOrderDetail += "公司 - " + drug.code + " - " + drug.name + " ( " + drug.count1kg + " )\n";
                        }

                        foreach (var drug in food.detailSP.drugSelfList)
                        {
                            messageOrderDetail += "自備 - " + drug.code + " - " + drug.name + " ( " + drug.count1kg + " )\n";
                        }
                        messageOrderDetail += "\n";
                    }
                }

                messageOrderDetail += "\n訂單備註：\n";
                messageOrderDetail += reqBody.note + "\n";
                messageOrderDetail += "訂單建立日期：" + now.ToString("yyyy/MM/dd HH:mm");

                // 新增訂單訊息 客戶下單 業務和客戶都收到訊息 ： 業務下單 只有業務看到訊息
                var userIds = new List<string>();
                userIds.Add(reqBody.tmcode);

                var message = new ordernotification()
                {
                    Message = messageOrderCustomer + messageOrderDetail,
                    Title = "[新增訂單][" + now.ToString("yyyy/MM/dd") + "] 客戶 " + mainCustomer.customer_sub_name + " (" + mainCustomer.customer_sub_id + ") 一筆新單",
                    CreateTime = DateTime.Now,
                    Tmcode = reqBody.tmcode
                };
                db.ordernotification.Add(message);

                if (!reqBody.orderPerson.Contains(mainCustomer.tmcode))  //業務下單 只有業務看到訊息
                {
                    userIds.Add(mainCustomer.customer_main_id);
                    var message2 = new ordernotification()
                    {
                        Message = messageOrderCustomer + messageOrderDetail,
                        Title = "[新增訂單][" + now.ToString("yyyy/MM/dd") + "] 客戶 " + mainCustomer.customer_sub_name + " (" + mainCustomer.customer_sub_id + ") 一筆新單",
                        CreateTime = DateTime.Now,
                        Tmcode = mainCustomer.customer_main_id

                    };
                    db.ordernotification.Add(message2);
                }
                await db.SaveChangesAsync();

                // 推播訊息
                await new NotificationApiController().orderNotification(new Models.NotificationModels.ResponseBody.OrderMessageReqBody
                {
                    userIds = userIds,
                    title = "新增訂單",
                    message = "客戶 " + reqBody.customerName + " (" + reqBody.customerId + ") 一筆新單",
                });
            }
            catch (Exception ex)
            {
                //resBody.error = ex.ToString();
                log.Error("addOrder:新增訂單失敗, " + new JavaScriptSerializer().Serialize(reqBody) + ", " + ExceptionExtension.GetaAllMessages(ex));
                resBody.error = "系統忙碌中";
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return resBody;
        }

        [DeflateCompression]
        [HttpPost]
        [Route("editOrder")]
        [JwtAuthActionFilter]
        public async Task<EditOrderResBody> editOrder(EditOrderReqBody reqBody)
        {
            cargillEntities db = null;
            EditOrderResBody resBody = null;
            cargillEntities originalDb = null;

            try
            {
                db = new cargillEntities();
                resBody = new EditOrderResBody();
                var originalModifyTime = reqBody.modify_time;

                if (reqBody.location == null | reqBody.date == null | reqBody.time == null || reqBody.foods.Count == 0)
                {
                    //throw new Exception("訂單欄位有缺");
                    resBody.error = "訂單欄位有缺";
                    return resBody;
                }

                var order = await db.customer_order.Where(a => a.customer_sub_id == reqBody.customerId && a.order_no == reqBody.orderNo)
                                                    .FirstOrDefaultAsync();
                var productdrugs = db.producttpdrug.ToList();

                if (order == null)
                {
                    resBody.error = "查無此訂單";
                    return resBody;
                }
                else
                {
                    var timeStr = "";
                    switch (reqBody.time)
                    {
                        case "上午":
                            timeStr = " 09:00";
                            break;
                        case "中午":
                            timeStr = " 12:00";
                            break;
                        case "下午":
                            timeStr = " 15:00";
                            break;
                        default:
                            timeStr = " " + reqBody.time;
                            break;
                    }

                    //對每個特配單設定出貨時間
                    var count = 1;
                    foreach (var food in reqBody.foods)
                    {
                        if (food.unit.Contains("散裝"))
                        {
                            food.weight = food.count;
                        }
                        else
                        {
                            food.weight = 30 * food.count / 1000;
                        }

                        if (food.detailSP == null) continue;
                        food.detailSP.deliveryDate = reqBody.date + timeStr;

                        //編輯時若有加新料，要補上detailSP的ID
                        if (string.IsNullOrEmpty(food.detailSP.id))
                        {
                            food.detailSP.id = DateTime.Now.ToString("HHmmss") + count.ToString().PadLeft(3, '0');
                        }
                        count++;
                    }

                    var details = JsonConvert.SerializeObject(reqBody.foods);
                    resBody.details = details;

                    var originalDbTime = order.modify_time;

                    order.details = details;
                    order.ship_to = reqBody.location;
                    order.modify_time = DateTime.Now;
                    order.delivery_date = DateTime.Parse(reqBody.date + timeStr);
                    order.desc_app = reqBody.note;
                    //customer_sub_id = reqBody.customerId;
                    //customer_sub_name = reqBody.customerName;
                    //customer_phone = reqBody.customerPhone,;
                    //order_person = reqBody.orderPerson;
                    //tmcode = reqBody.tmcode;

                    //type = "APP";
                    order.modify_logs = order.modify_logs + DateTime.Now.ToString("yyyyMMdd HH:mm") + " APP " + reqBody.orderPerson + " 儲存,";
                    //order_no = DateTime.Now.ToString("yyyyMMddHHmmss");

                    db.Entry(order).State = EntityState.Modified;

                    #region 訊息
                    // 訊息頁內文
                    var mainCustomer = await (from customer in db.customer
                                              where customer.customer_sub_id == reqBody.customerId
                                              select customer).FirstOrDefaultAsync();

                    //訂單
                    WebOrderModel[] foods = JsonConvert.DeserializeObject<WebOrderModel[]>(order.details);

                    var messageOrderCustomer = "";
                    var messageOrderDetail = "";

                    messageOrderCustomer += mainCustomer.tmcode + "您好，客戶 " + mainCustomer.customer_sub_name + "(" + mainCustomer.customer_sub_id + ") 修改一筆訂單\n";
                    messageOrderCustomer += "子帳號:" + mainCustomer.customer_sub_name + "(" + mainCustomer.customer_sub_id + ")\n";
                    messageOrderCustomer += "主帳號:" + mainCustomer.customer_main_name + "(" + mainCustomer.customer_main_id + ")\n\n";

                    var deliverData = order.delivery_date;
                    messageOrderDetail += "希望到貨日期：" + deliverData.Value.ToString("yyyy/MM/dd") + " " + deliverData.Value.ToString("HH:mm") + "\n";
                    messageOrderDetail += "送貨地點：" + order.ship_to + "\n\n";

                    foreach (var food in foods)
                    {
                        messageOrderDetail += food.code + " - " + food.name + " - " + food.unit + " ( " + food.count + " )\n";
                        
                        if (food.detailSP != null)
                        {
                            if (food.detailSP.drugCompanyList != null)
                            {
                                foreach (var drug in food.detailSP.drugCompanyList)
                                {
                                    messageOrderDetail += "公司 - " + drug.code + " - " + drug.name + " ( " + drug.count1kg + " )\n";
                                }
                            }

                            if (food.detailSP.drugSelfList != null)
                            {
                                foreach (var drug in food.detailSP.drugSelfList)
                                {
                                    messageOrderDetail += "自備 - " + drug.code + " - " + drug.name + " ( " + drug.count1kg + " )\n";
                                }
                            }

                            if (food.detailSP.drugCompanyList != null)
                            {
                                //塞初始資料
                                foreach (var drugCompany in food.detailSP.drugCompanyList)
                                {
                                    var productdrug = productdrugs.Where(x => x.code == drugCompany.code).FirstOrDefault();
                                    drugCompany.count2kg = drugCompany.count1kg * 2;
                                    drugCompany.recommend = !string.IsNullOrEmpty(productdrug.recommended_amount) ? productdrug.recommended_amount : "/";

                                    drugCompany.totalCount = food.weight * drugCompany.count1kg;

                                    drugCompany.days = productdrug.drug_withdrawal;
                                    drugCompany.note = productdrug.note;
                                    drugCompany.price = 0;
                                }

                                foreach (var drugSelf in food.detailSP.drugSelfList)
                                {
                                    var productdrug = productdrugs.Where(x => x.name == drugSelf.name).FirstOrDefault();
                                    if (productdrug != null)
                                    {
                                        drugSelf.count2kg = drugSelf.count1kg * 2;
                                        drugSelf.recommend = !string.IsNullOrEmpty(productdrug.recommended_amount) ? productdrug.recommended_amount : "/";

                                        drugSelf.totalCount = food.weight * drugSelf.count1kg;

                                        drugSelf.days = productdrug.drug_withdrawal;
                                        drugSelf.note = productdrug.note;
                                        drugSelf.price = 0;
                                    }
                                }
                            }
                        }

                        messageOrderDetail += "\n";
                    }

                    var now = DateTime.Now;
                    messageOrderDetail += "\n訂單備註：\n";
                    messageOrderDetail += reqBody.note + "\n";
                    messageOrderDetail += "訂單建立日期：" + order.create_time.ToString("yyyy/MM/dd HH:mm") + "\n";
                    messageOrderDetail += "訂單修改日期：" + now.ToString("yyyy/MM/dd HH:mm");

                    // 新增訂單訊息
                    var userIds = new List<string>();
                    userIds.Add(mainCustomer.tmcode);

                    var message = new ordernotification()
                    {
                        Message = messageOrderCustomer + messageOrderDetail,
                        Title = "[修改訂單][" + now.ToString("yyyy/MM/dd") + "] 客戶 " + mainCustomer.customer_sub_name + " (" + mainCustomer.customer_sub_id + ") 修改一筆訂單",
                        CreateTime = DateTime.Now,
                        Tmcode = mainCustomer.tmcode,

                    };
                    db.ordernotification.Add(message);
                    if (!reqBody.orderPerson.Contains(mainCustomer.tmcode))  //業務下單 只有業務看到訊息
                    {
                        userIds.Add(mainCustomer.customer_main_id);
                        var message2 = new ordernotification()
                        {
                            Message = messageOrderCustomer + messageOrderDetail,
                            Title = "[修改訂單][" + now.ToString("yyyy/MM/dd") + "] 客戶 " + mainCustomer.customer_sub_name + " (" + mainCustomer.customer_sub_id + ") 修改一筆訂單",
                            CreateTime = DateTime.Now,
                            Tmcode = mainCustomer.customer_main_id,

                        };
                        db.ordernotification.Add(message2);
                    }

                    #region 再撈一次DB 檢查modify欄位
                    originalDb = new cargillEntities();
                    var originalOrder = await originalDb.customer_order.Where(a => a.customer_sub_id == reqBody.customerId && a.order_no == reqBody.orderNo)
                                    .FirstOrDefaultAsync();
                    if(originalModifyTime != originalOrder.modify_time)
                    {
                        throw new Exception("訂單已被調整，請重新下訂單");
                    }
                    #endregion

                    await db.SaveChangesAsync();

                    // 推播訊息
                    await new NotificationApiController().orderNotification(new Models.NotificationModels.ResponseBody.OrderMessageReqBody
                    {
                        userIds = userIds,
                        title = "修改訂單",
                        message = "客戶 " + order.customer_sub_name + " (" + reqBody.customerId + ") 修改一筆訂單",
                    });
                    #endregion
                }
            }
            catch (Exception ex)
            {
                //resBody.error = ex.ToString();
                log.Error("editOrder:編輯訂單失敗, " + new JavaScriptSerializer().Serialize(reqBody) + ", " + ExceptionExtension.GetaAllMessages(ex));
                resBody.error = "系統忙碌中";
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
                if (originalDb != null)
                {
                    originalDb.Dispose();
                    originalDb = null;
                }
            }

            return resBody;
        }

        [DeflateCompression]
        [HttpPost]
        [Route("DeleteOrder")]
        [JwtAuthActionFilter]
        public async Task<DeleteOrderResBody> deleteOrder(DeleteOrderReqBody reqBody)
        {
            cargillEntities db = null;
            DeleteOrderResBody resBody = null;

            try
            {
                db = new cargillEntities();
                resBody = new DeleteOrderResBody();

                var order = await db.customer_order.Where(a => a.customer_sub_id == reqBody.customerId
                                                        && a.order_no == reqBody.orderNo
                                                        && a.type == "APP").FirstOrDefaultAsync();

                if (order == null)
                {
                    resBody.error = "查無此訂單";
                    return resBody;
                }
                else
                {
                    //改狀態
                    order.modify_time = DateTime.Now;
                    order.type = "DEL";
                    order.modify_logs = order.modify_logs + DateTime.Now.ToString("yyyyMMdd HH:mm") + " APP " + reqBody.orderPerson + " 刪除,";

                    db.Entry(order).State = EntityState.Modified;

                    //直接刪除
                    //db.Entry(order).State = EntityState.Deleted;

                    #region 訊息
                    // 訊息頁內文
                    var mainCustomer = await (from customer in db.customer
                                              where customer.customer_sub_id == reqBody.customerId
                                              select customer).FirstOrDefaultAsync();

                    //訂單
                    WebOrderModel[] foods = JsonConvert.DeserializeObject<WebOrderModel[]>(order.details);

                    var messageOrderCustomer = "";
                    var messageOrderDetail = "";

                    messageOrderCustomer += mainCustomer.tmcode + "您好，客戶 " + mainCustomer.customer_sub_name + "(" + mainCustomer.customer_sub_id + ") 刪除一筆訂單\n";
                    messageOrderCustomer += "子帳號:" + mainCustomer.customer_sub_name + "(" + mainCustomer.customer_sub_id + ")\n";
                    messageOrderCustomer += "主帳號:" + mainCustomer.customer_main_name + "(" + mainCustomer.customer_main_id + ")\n\n";

                    var deliverData = order.delivery_date;
                    messageOrderDetail += "希望到貨日期：" + deliverData.Value.ToString("yyyy/MM/dd") + " " + deliverData.Value.ToString("HH:mm") + "\n";
                    messageOrderDetail += "送貨地點：" + order.ship_to + "\n\n";

                    foreach (var food in foods)
                    {
                        messageOrderDetail += food.code + " - " + food.name + " - " + food.unit + " ( " + food.count + " )\n";
                        
                        if (food.detailSP != null && food.detailSP.drugCompanyList != null)
                        {
                            foreach (var drug in food.detailSP.drugCompanyList)
                            {
                                messageOrderDetail += "公司 - " + drug.code + " - " + drug.name + " ( " + drug.count1kg + " )\n";
                            }
                        }

                        if (food.detailSP != null && food.detailSP.drugSelfList != null)
                        {
                            foreach (var drug in food.detailSP.drugSelfList)
                            {
                                messageOrderDetail += "自備 - " + drug.code + " - " + drug.name + " ( " + drug.count1kg + " )\n";
                            }
                        }
                        messageOrderDetail += "\n";
                    }

                    var now = DateTime.Now;
                    messageOrderDetail += "\n訂單備註：\n";
                    messageOrderDetail += order.desc_app + "\n";
                    messageOrderDetail += "訂單建立日期：" + order.create_time.ToString("yyyy/MM/dd HH:mm") + "\n";
                    messageOrderDetail += "訂單刪除日期：" + now.ToString("yyyy/MM/dd HH:mm");

                    // 新增訂單訊息
                    var userIds = new List<string>();
                    userIds.Add(mainCustomer.tmcode);
                    var message = new ordernotification()
                    {
                        Message = messageOrderCustomer + messageOrderDetail,
                        Title = "[刪除訂單][" + now.ToString("yyyy/MM/dd") + "] 客戶 " + mainCustomer.customer_sub_name + " (" + mainCustomer.customer_sub_id + ") 刪除一筆訂單",
                        CreateTime = DateTime.Now,
                        Tmcode = mainCustomer.tmcode,

                    };
                    db.ordernotification.Add(message);
                    if (!reqBody.orderPerson.Contains(mainCustomer.tmcode))  //業務下單 只有業務看到訊息
                    {
                        userIds.Add(mainCustomer.customer_main_id);
                        var message2 = new ordernotification()
                        {
                            Message = messageOrderCustomer + messageOrderDetail,
                            Title = "[刪除訂單][" + now.ToString("yyyy/MM/dd") + "] 客戶 " + mainCustomer.customer_sub_name + " (" + mainCustomer.customer_sub_id + ") 刪除一筆訂單",
                            CreateTime = DateTime.Now,
                            Tmcode = mainCustomer.customer_main_id,

                        };
                        db.ordernotification.Add(message2);
                    }

                    await db.SaveChangesAsync();

                    // 推播訊息
                    await new NotificationApiController().orderNotification(new Models.NotificationModels.ResponseBody.OrderMessageReqBody
                    {
                        userIds = userIds,
                        title = "刪除訂單",
                        message = "客戶 " + order.customer_sub_name + " (" + reqBody.customerId + ") 刪除一筆訂單",
                    });
                    #endregion

                }
            }
            catch (Exception ex)
            {
                //resBody.error = ex.ToString();
                log.Error("deleteOrder:刪除訂單失敗, " + new JavaScriptSerializer().Serialize(reqBody) + ", " + ExceptionExtension.GetaAllMessages(ex));
                resBody.error = "系統忙碌中";
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return resBody;
        }

        [DeflateCompression]
        [HttpPost]
        [Route("SendOrderEndMessage")]
        public async Task<SendOrderEndMessageResBody> sendOrderEndMessage(SendOrderEndMessageReqBody reqBody)
        {
            cargillEntities db = null;
            SendOrderEndMessageResBody resBody = null;

            try
            {
                db = new cargillEntities();
                resBody = new SendOrderEndMessageResBody();

                if (reqBody.poCode == null || reqBody.poCode == "" || reqBody.type != "WEB_END")
                {
                    resBody.error = "查無此訂單";
                    return resBody;
                }

                var order = await db.customer_order.Where(a => a.customer_sub_id == reqBody.customerId
                                                        && a.order_no == reqBody.orderNo).FirstOrDefaultAsync();

                if (order == null)
                {
                    resBody.error = "查無此訂單";
                    return resBody;
                }
                else
                {

                    #region 訊息
                    // 訊息頁內文
                    var mainCustomer = await (from customer in db.customer
                                              where customer.customer_sub_id == reqBody.customerId
                                              select customer).FirstOrDefaultAsync();

                    //訂單
                    WebOrderModel[] foods = JsonConvert.DeserializeObject<WebOrderModel[]>(order.details);

                    var messageOrderCustomer = "";
                    var messageOrderDetail = "";

                    messageOrderCustomer += mainCustomer.tmcode + "您好，客戶 " + mainCustomer.customer_sub_name + "(" + mainCustomer.customer_sub_id + ") 完成一筆訂單\n";
                    messageOrderCustomer += "子帳號:" + mainCustomer.customer_sub_name + "(" + mainCustomer.customer_sub_id + ")\n";
                    messageOrderCustomer += "主帳號:" + mainCustomer.customer_main_name + "(" + mainCustomer.customer_main_id + ")\n\n";

                    var deliverData = order.delivery_date;
                    messageOrderDetail += "希望到貨日期：" + deliverData.Value.ToString("yyyy/MM/dd") + " " + deliverData.Value.ToString("HH:mm") + "\n";
                    messageOrderDetail += "送貨地點：" + order.ship_to + "\n\n";

                    foreach (var food in foods)
                    {
                        messageOrderDetail += food.code + " - " + food.name + " - " + food.unit + " ( " + food.count + " )\n";

                        if (food.detailSP != null && food.detailSP.drugCompanyList != null)
                        {
                            foreach (var drug in food.detailSP.drugCompanyList)
                            {
                                messageOrderDetail += "公司 - " + drug.code + " - " + drug.name + " ( " + drug.count1kg + " )\n";
                            }
                        }

                        if (food.detailSP != null && food.detailSP.drugSelfList != null)
                        {
                            foreach (var drug in food.detailSP.drugSelfList)
                            {
                                messageOrderDetail += "自備 - " + drug.code + " - " + drug.name + " ( " + drug.count1kg + " )\n";
                            }
                        }
                        messageOrderDetail += "\n";
                    }

                    var now = DateTime.Now;
                    messageOrderDetail += "\n訂單備註：\n";
                    messageOrderDetail += order.desc_app + "\n";
                    messageOrderDetail += "訂單建立日期：" + order.create_time.ToString("yyyy/MM/dd HH:mm") + "\n";
                    messageOrderDetail += "訂單完成日期：" + now.ToString("yyyy/MM/dd HH:mm");

                    // 新增訂單訊息
                    var userIds = new List<string>();
                    userIds.Add(mainCustomer.tmcode);
                    var message = new ordernotification()
                    {
                        Message = messageOrderCustomer + messageOrderDetail,
                        Title = "[完成訂單][" + now.ToString("yyyy/MM/dd") + "] 客戶 " + mainCustomer.customer_sub_name + " (" + mainCustomer.customer_sub_id + ") 完成一筆訂單",
                        CreateTime = DateTime.Now,
                        Tmcode = mainCustomer.tmcode,

                    };
                    db.ordernotification.Add(message);
                    if (!order.order_person.Contains(mainCustomer.tmcode))  //業務下單 只有業務看到訊息
                    {
                        userIds.Add(mainCustomer.customer_main_id);
                        var message2 = new ordernotification()
                        {
                            Message = messageOrderCustomer + messageOrderDetail,
                            Title = "[完成訂單][" + now.ToString("yyyy/MM/dd") + "] 客戶 " + mainCustomer.customer_sub_name + " (" + mainCustomer.customer_sub_id + ") 完成一筆訂單",
                            CreateTime = DateTime.Now,
                            Tmcode = mainCustomer.customer_main_id,

                        };
                        db.ordernotification.Add(message2);
                    }

                    await db.SaveChangesAsync();

                    // 推播訊息
                    await new NotificationApiController().orderNotification(new Models.NotificationModels.ResponseBody.OrderMessageReqBody
                    {
                        userIds = userIds,
                        title = "完成訂單",
                        message = "客戶 " + order.customer_sub_name + " (" + reqBody.customerId + ") 完成一筆訂單",
                    });
                    #endregion

                }
            }
            catch (Exception ex)
            {
                //resBody.error = ex.ToString();
                resBody.error = "系統忙碌中";
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return resBody;
        }

        private CanDeliveredTime getCanDeliveredTime()
        {
            var canDeliveredyHours = new List<string>();
            var now = DateTime.Now;
            //TimeSpan aaa = new TimeSpan(8, 20, 0);
            //now = now.Date + aaa;
            var canDeliveredDate = now.AddHours(int.Parse(ConfigurationManager.AppSettings["OrderCanDeliveredHour"]) + 1);
            if (canDeliveredDate.Hour > 18)
            {
                canDeliveredDate = canDeliveredDate.AddDays(1);
                TimeSpan ts = new TimeSpan(6, 0, 0);
                canDeliveredDate = canDeliveredDate.Date + ts;  //隔天的 06:00                
            }
            while (canDeliveredDate.Hour <= 18) //最多只能訂 06:00 - 18:00
            {
                canDeliveredyHours.Add(canDeliveredDate.ToString("HH:00"));
                canDeliveredDate = canDeliveredDate.AddHours(1);
            }
            return new CanDeliveredTime()
            {
                canDeliveredDate = canDeliveredDate.ToString("yyyy-MM-dd"),
                canDeliveredyHours = canDeliveredyHours
            };
        }
    }
}
