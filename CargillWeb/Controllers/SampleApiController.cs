﻿using CargillWeb.Models;
using CargillWeb.Models.DBModels;
using CargillWeb.Models.SampleModel.RequestBody;
using CargillWeb.Models.SampleModel.ResponseBody;
using CargillWeb.Models.UserModel.RequestBody;
using CargillWeb.Models.UserModel.ResponseBody;
using CargillWeb.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CargillWeb.Controllers
{
    [EnableCors("*", "*", "*")]
    [RoutePrefix("api/sample")]
    public class SampleApiController : ApiController
    {
        [DeflateCompression]
        [HttpGet]
        [Route("getProduct")]
        public async Task<GetProductResBody> getProduct()
        {
            cargillEntities db = null;
            GetProductResBody resBody = null;

            try
            {
                db = new cargillEntities();
                resBody = new GetProductResBody();

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }, TransactionScopeAsyncFlowOption.Enabled))
                {
                    var products = await db.product.ToListAsync();
                    resBody.products = products;
                }
            }
            catch (Exception ex)
            {
                //resBody.error = ex.ToString();
                resBody.error = "系統忙碌中";
            }
            finally
            {
                db.Dispose();
            }
            
            return resBody;
        }

        /*
        [DeflateCompression]
        [HttpPost]
        [Route("addAppUser")]
        public async Task<AddAppUserResBody> addAppUser(AddAppUserReqBody reqBody)
        {
            cargillEntities db = null;
            AddAppUserResBody resBody = null;

            try
            {
                db = new cargillEntities();
                resBody = new AddAppUserResBody();

                var userData = await (from user in db.app_user
                                        where user.unicode == reqBody.unicode
                                        select user).FirstOrDefaultAsync();

                if (userData != null)
                {
                    resBody.error = "已有帳號";
                        
                }
                else
                {
                    userData = new app_user()
                    {
                        create_time = DateTime.Now,
                        phone = reqBody.phone,
                        type = reqBody.type,
                        unicode = reqBody.unicode,
                        user_name = reqBody.user_name,
                    };
                    db.app_user.Add(userData);
                    //resBody.error = "有帳號";
                    //userData.user_name = "app測試";
                    //db.Entry(userData).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                resBody.error = ex.ToString();
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return resBody;
        }*/

        [DeflateCompression]
        [HttpGet]
        [Route("getSlides")]
        public async Task<GetSlidesResBody> getSlides()
        {
            cargillEntities db = null;
            GetSlidesResBody resBody = null;

            try
            {
                db = new cargillEntities();
                resBody = new GetSlidesResBody();

                var slides = await db.marketing_file.FirstOrDefaultAsync();

                resBody.time = slides.carousel_sec;

                if (slides.file_name1.Trim() != "")
                {
                    resBody.slides.Add(new slidesData()
                    {
                        file = "marketing/download/1",
                        link = slides.file_link1.Trim() != "" ? slides.file_link1 : "",
                    });
                }

                if (slides.file_name2.Trim() != "")
                {
                    resBody.slides.Add(new slidesData()
                    {
                        file = "marketing/download/2",
                        link = slides.file_link2.Trim() != "" ? slides.file_link2 : "",
                    });
                }

                if (slides.file_name3.Trim() != "")
                {
                    resBody.slides.Add(new slidesData()
                    {
                        file = "marketing/download/3",
                        link = slides.file_link3.Trim() != "" ? slides.file_link3 : "",
                    });
                }

                if (slides.file_name4.Trim() != "")
                {
                    resBody.slides.Add(new slidesData()
                    {
                        file = "marketing/download/4",
                        link = slides.file_link4.Trim() != "" ? slides.file_link4 : "",
                    });
                }

                if (slides.file_name5.Trim() != "")
                {
                    resBody.slides.Add(new slidesData()
                    {
                        file = "marketing/download/5",
                        link = slides.file_link5.Trim() != "" ? slides.file_link5 : "",
                    });
                }

            }
            catch (Exception ex)
            {
                //resBody.error = ex.ToString();
                resBody.error = "系統忙碌中";
            }
            finally
            {
                db.Dispose();
            }

            return resBody;
        }

    }
}
