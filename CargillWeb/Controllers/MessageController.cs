﻿using CargillWeb.Models.DBModels;
using CargillWeb.Models.MessageModel.RequestBody;
using CargillWeb.Models.MessageModel.ResponseBody;
using CargillWeb.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CargillWeb.Controllers
{
    [EnableCors("*", "*", "*")]
    [RoutePrefix("api/message")]
    public class MessageController : ApiController
    {
        const int PrepareState = 1;
        const int PushingState = 2;
        const int DoneState = 3;
        const int CancelState = 4;
        const int FailState = 5;

        const int TypeAllImmediate = 0;
        const int TypeAllSchedule = 1;
        const int TypeListImmediate = 2;
        const int TypeListSchedule = 3;
        const int TypeOrderMessage = 4;

        [DeflateCompression]
        [HttpPost]
        [Route("getMessages")]
        [JwtAuthActionFilter]
        public async Task<GetMessagesResBody> getMessages(GetMessagesReqBody reqBody)
        {
            cargillEntities db = null;
            GetMessagesResBody resBody = null;

            try
            {
                db = new cargillEntities();
                resBody = new GetMessagesResBody();

                var nowTime = DateTime.Now.AddMonths(-3);
                var messages = await db.notification.Where(a => (a.Type == TypeAllImmediate || a.Type == TypeAllSchedule) && a.State == DoneState && DateTime.Compare(nowTime, a.CreateTime) < 0)
                                                    .OrderByDescending(b => b.CreateTime).ToListAsync();

                foreach (var message in messages)
                {
                    string id = message.Id.ToString();
                    resBody.messages.Add(new message()
                    {
                        messageId = id,
                        content = message.Message,
                        title = message.Title,
                        createTime = message.CreateTime,
                    });

                }

                List<ordernotification> orderMessages = new List<ordernotification>();
                if (reqBody.userType == "S") //業務
                {
                    orderMessages = await db.ordernotification.Where(a => a.Tmcode == reqBody.userId && DateTime.Compare(nowTime, a.CreateTime) < 0)
                                                                           .OrderByDescending(b => b.CreateTime).ToListAsync();
                } else if(reqBody.userType == "SC")
                {
                    var subCustomer = (from customer in db.customer
                                        where customer.customer_sub_id == reqBody.userId
                                        select customer).FirstOrDefault();
                    if(subCustomer != null)
                    {
                        orderMessages = await db.ordernotification.Where(a => a.Tmcode == subCustomer.customer_main_id && DateTime.Compare(nowTime, a.CreateTime) < 0)
                                                                               .OrderByDescending(b => b.CreateTime).ToListAsync();
                    }
                }
                else if (reqBody.userType == "MC")
                {
                    orderMessages = await db.ordernotification.Where(a => a.Tmcode == reqBody.userId && DateTime.Compare(nowTime, a.CreateTime) < 0)
                                                                              .OrderByDescending(b => b.CreateTime).ToListAsync();
                }

                foreach (var message in orderMessages)
                {
                    string id = message.Id.ToString();
                    resBody.messages.Add(new message()
                    {
                        messageId = "order_" + id,
                        content = message.Message,
                        title = message.Title,
                        createTime = message.CreateTime,
                    });

                }

                resBody.messages = resBody.messages.OrderByDescending(x => x.createTime).ToList();
            }
            catch (Exception ex)
            {
                //resBody.error = ex.ToString();
                resBody.error = "系統忙碌中";
            }
            finally
            {
                db.Dispose();
            }

            return resBody;
        }

        [DeflateCompression]
        [HttpPost]
        [Route("addOrderMessage")]
        [JwtAuthActionFilter]
        public async Task<AddOrderMessageResBody> addOrder(AddOrderMessageReqBody reqBody)
        {
            cargillEntities db = null;
            AddOrderMessageResBody resBody = null;

            try
            {
                db = new cargillEntities();
                resBody = new AddOrderMessageResBody();

                var message = new ordernotification()
                {
                    Message = "客戶 " + reqBody.customerId + " 新增了一筆訂單",
                    Title = "新增訂單",
                    CreateTime = DateTime.Now,
                    Tmcode = reqBody.tmcode,

                };
                db.ordernotification.Add(message);
                await db.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                //resBody.error = ex.ToString();
                resBody.error = "系統忙碌中";
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return resBody;
        }

        [DeflateCompression]
        [HttpPost]
        [Route("addBookingMessage")]
        [JwtAuthActionFilter]
        public async Task<AddOrderMessageResBody> addBookingMessage(AddOrderMessageReqBody reqBody)
        {
            cargillEntities db = null;
            AddOrderMessageResBody resBody = null;

            try
            {
                db = new cargillEntities();
                resBody = new AddOrderMessageResBody();

                var message = new ordernotification()
                {
                    Message = "客戶 " + reqBody.customerId + " 新增了一筆訂單",
                    Title = "新增訂單",
                    CreateTime = DateTime.Now,
                    Tmcode = reqBody.tmcode,

                };
                db.ordernotification.Add(message);
                await db.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                //resBody.error = ex.ToString();
                resBody.error = "系統忙碌中";
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return resBody;
        }

        [DeflateCompression]
        [HttpPost]
        [Route("deleteMessages")]
        [JwtAuthActionFilter]
        public async Task<DeleteMessagesResBody> deleteMessages(DeleteMessageReqBody reqBody)
        {
            cargillEntities db = null;
            DeleteMessagesResBody resBody = null;

            try
            {
                db = new cargillEntities();
                resBody = new DeleteMessagesResBody();

                List<int> tempIds = new List<int>();
                foreach (var id in reqBody.messageIds)
                {
                    int index = id.IndexOf("_");
                    tempIds.Add(Int32.Parse(id.Substring(index + 1)));
                }

                List<ordernotification> orderMessages = new List<ordernotification>();
                if (reqBody.userType == "S") //業務
                {
                    orderMessages = await db.ordernotification.Where(a => a.Tmcode == reqBody.userId && tempIds.Contains(a.Id))
                                                                           .OrderByDescending(b => b.CreateTime).ToListAsync();
                }
                else if (reqBody.userType == "SC")
                {
                    var subCustomer = (from customer in db.customer
                                       where customer.customer_sub_id == reqBody.userId
                                       select customer).FirstOrDefault();
                    if (subCustomer != null)
                    {
                        orderMessages = await db.ordernotification.Where(a => a.Tmcode == subCustomer.customer_main_id && tempIds.Contains(a.Id))
                                                                               .OrderByDescending(b => b.CreateTime).ToListAsync();
                    }
                }
                else if (reqBody.userType == "MC")
                {
                    orderMessages = await db.ordernotification.Where(a => a.Tmcode == reqBody.userId && tempIds.Contains(a.Id))
                                                                              .OrderByDescending(b => b.CreateTime).ToListAsync();
                }

                if (orderMessages == null)
                {
                    resBody.error = "查無此訊息";
                    return resBody;
                }
                else
                {
                    db.ordernotification.RemoveRange(orderMessages);
                    await db.SaveChangesAsync();
                }

            }
            catch (Exception ex)
            {
                //resBody.error = ex.ToString();
                resBody.error = "系統忙碌中";
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return resBody;
        }
    }
}
