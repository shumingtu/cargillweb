﻿using CargillWeb.Models.DBModels;
using CargillWeb.Models.NotificationModels;
using CargillWeb.Models.NotificationModels.RequestBody;
using CargillWeb.Models.NotificationModels.ResponseBody;
using CargillWeb.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CargillWeb.Controllers
{
    [EnableCors("*", "*", "*")]
    [RoutePrefix("api/notification")]
    public class NotificationApiController : ApiController
    {
        const int PrepareState = 1;
        const int PushingState = 2;
        const int DoneState = 3;
        const int CancelState = 4;
        const int FailState = 5;

        const int TypeAllImmediate = 0;
        const int TypeAllSchedule = 1;
        const int TypeListImmediate = 2;
        const int TypeListSchedule = 3;
        const int TypeOrderMessage = 4;

        [HttpGet]
        [Route("")]
        public async Task<List<NotificationResBody>> getNotifications()
        {
            var resBody = new List<NotificationResBody>();
            var notifications = new List<notification>();

            cargillEntities db = null;
            try
            {
                db = new cargillEntities();

                notifications = await db.notification.AsNoTracking().ToListAsync();

                foreach (var notification in notifications)
                {
                    if (notification.Type != TypeOrderMessage)
                    {
                        var notificationResBody = new NotificationResBody()
                        {
                            Id = notification.Id,
                            Title = notification.Title,
                            Message = notification.Message,
                            CreateTime = notification.CreateTime,
                            ScheduleTime = notification.ScheduleTime,
                            PushTime = notification.PushTime,
                            Type = notification.Type,
                            State = notification.State
                        };

                        resBody.Add(notificationResBody);
                    }
                }
                await db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null) db.Dispose();
            }
            return resBody;
        }

        [HttpPost]
        [Route("")]
        public async Task<NotificationResBody> addNotification()
        {
            cargillEntities db = null;
            NotificationResBody notificationResBody = new NotificationResBody();
            var now = DateTime.Now;
            var path = "";

            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            string root = ConfigurationManager.AppSettings["SavePushTempFilePath"];
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            CustomMultipartFormDataStreamProvider provider = new CustomMultipartFormDataStreamProvider(root);

            try
            {
                db = new cargillEntities();
                NotificationReqBody notificationReqBody = new NotificationReqBody();
                await Request.Content.ReadAsMultipartAsync(provider);
                foreach (var key in provider.FormData.AllKeys)
                {
                    foreach (var val in provider.FormData.GetValues(key))
                    {
                        notificationReqBody = JsonConvert.DeserializeObject<NotificationReqBody>(val);
                    }
                }

                if (notificationReqBody.type == TypeAllImmediate || notificationReqBody.type == TypeAllSchedule)
                {
                    StringBuilder builder = new StringBuilder();
                    builder.AppendLine(ConfigurationManager.AppSettings["PushAllTarget"]);
                    path = ConfigurationManager.AppSettings["SavePushFilePath"] + ConfigurationManager.AppSettings["PushAllFile"];
                    File.WriteAllText(path, builder.ToString());
                }
                else
                {
                    var result = SaveFileService.UploadPushFile(provider);
                    path = result.path;

                    foreach(var userId in result.userIds)
                    {
                        var orderNotification = new ordernotification()
                        {
                            Title = notificationReqBody.title,
                            Message = notificationReqBody.message,
                            Type = 1,
                            CreateTime = now,
                            Tmcode = userId
                        };
                        db.ordernotification.Add(orderNotification);
                    }
                }

                var notification = new notification()
                {
                    Title = notificationReqBody.title,
                    Message = notificationReqBody.message,
                    CreateTime = now,
                    ScheduleTime = notificationReqBody.scheduleTime,
                    Type = notificationReqBody.type,
                    State = PrepareState,
                    FilePath = path
                };
                db.notification.Add(notification);
                await db.SaveChangesAsync();

                PushDirectModel pushDirectModel = new PushDirectModel();
                pushDirectModel.id = notification.Id;
                pushDirectModel.title = notificationReqBody.title;
                pushDirectModel.message = notificationReqBody.message;

                HttpStatusCode pushStatusCode = new HttpStatusCode();

                if (notificationReqBody.type == TypeAllSchedule || notificationReqBody.type == TypeListSchedule)
                {
                    PushBatchModel pushBatchModel = new PushBatchModel();
                    pushBatchModel.pushnotification = pushDirectModel;

                    PushScheduleModel pushScheduleModel = new PushScheduleModel();
                    pushScheduleModel.batchpushobject = pushBatchModel;
                    pushScheduleModel.schedule = notificationReqBody.scheduleTime;
                    pushBatchModel.path = notification.FilePath;

                    pushStatusCode = await PushNotificationService.PushSchedule(pushScheduleModel);
                }
                else
                {
                    if (notificationReqBody.type == TypeAllImmediate)
                    {
                        List<string> targets = new List<string>();
                        targets.Add(ConfigurationManager.AppSettings["PushAllTarget"]);
                        pushDirectModel.targets = targets;
                        pushStatusCode = await PushNotificationService.Push(pushDirectModel);
                    }
                    else
                    {
                        PushBatchModel pushBatchModel = new PushBatchModel();
                        pushBatchModel.pushnotification = pushDirectModel;
                        pushBatchModel.path = notification.FilePath;
                        pushStatusCode = await PushNotificationService.PushBatch(pushBatchModel);
                    }
                }
                if (pushStatusCode != HttpStatusCode.OK)
                {
                    notification.State = FailState;
                    db.Entry(notification).State = EntityState.Modified;
                    notificationResBody.Error = "推播失敗";
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                //notificationResBody.Error = ex.ToString();
                notificationResBody.Error = "系統忙碌中";
            }
            finally
            {
                if (db != null) db.Dispose();
            }
            return notificationResBody;
        }

        [HttpPut]
        [Route("")]
        public async Task updateNotification(NotificationReqBody reqBody)
        {
            cargillEntities db = null;
            try
            {
                db = new cargillEntities();
                var notification = await db.notification.Where(x => x.Id == reqBody.id).FirstOrDefaultAsync();
                notification.Message = reqBody.message;
                db.Entry(notification).State = EntityState.Modified;

                await db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null) db.Dispose();
            }
        }

        [HttpDelete]
        [Route("{id:int}")]
        public async Task deleteNotification(int id)
        {
            cargillEntities db = null;
            try
            {
                db = new cargillEntities();
                var notification = await db.notification.Where(x => x.Id == id).FirstOrDefaultAsync();
                if (notification.State == PrepareState)
                {
                    notification.State = CancelState;
                    db.Entry(notification).State = EntityState.Modified;
                }
                else
                {
                    db.notification.Remove(notification);
                    db.Entry(notification).State = EntityState.Deleted;
                }
                await db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null) db.Dispose();
            }
        }

        [HttpPost]
        [Route("orderMessage")]
        public async Task orderNotification(OrderMessageReqBody reqBody)
        {
            cargillEntities db = null;
            try
            {
                db = new cargillEntities();
                var path = await SaveFileService.SaveOrderMessage(reqBody.userIds);

                var notification = new notification()
                {
                    Title = reqBody.title,
                    Message = reqBody.message,
                    CreateTime = DateTime.Now,
                    Type = TypeOrderMessage,
                    State = PrepareState,
                    FilePath = path
                };
                db.notification.Add(notification);
                await db.SaveChangesAsync();

                PushDirectModel pushDirectModel = new PushDirectModel();
                pushDirectModel.id = notification.Id;
                pushDirectModel.title = reqBody.title;
                pushDirectModel.message = reqBody.message;

                HttpStatusCode pushStatusCode = new HttpStatusCode();

                PushBatchModel pushBatchModel = new PushBatchModel();
                pushBatchModel.pushnotification = pushDirectModel;
                pushBatchModel.path = notification.FilePath;
                pushStatusCode = await PushNotificationService.PushBatch(pushBatchModel);

                if (pushStatusCode != HttpStatusCode.OK)
                {
                    notification.State = FailState;
                    db.Entry(notification).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null) db.Dispose();
            }

        }

        [HttpPost]
        [Route("bookingMessage")]
        public async Task<Boolean> bookingMessage(BookingMessageReqBody reqBody)
        {
            cargillEntities db = null;
            PushResBody pushResBody = new PushResBody();
            try
            {
                db = new cargillEntities();
                var path = await SaveFileService.SaveOrderMessage(reqBody.userIds);

                var notification = new notification()
                {
                    Title = reqBody.title,
                    Message = reqBody.message,
                    CreateTime = DateTime.Now,
                    Type = TypeOrderMessage,
                    State = PrepareState,
                    FilePath = path
                };
                db.notification.Add(notification);
                await db.SaveChangesAsync();

                PushDirectModel pushDirectModel = new PushDirectModel();
                pushDirectModel.id = notification.Id;
                pushDirectModel.title = reqBody.title;
                pushDirectModel.message = reqBody.message;

                HttpStatusCode pushStatusCode = new HttpStatusCode();

                PushBatchModel pushBatchModel = new PushBatchModel();
                pushBatchModel.pushnotification = pushDirectModel;
                pushBatchModel.path = notification.FilePath;
                pushStatusCode = await PushNotificationService.PushBatch(pushBatchModel);

                if (pushStatusCode != HttpStatusCode.OK)
                {
                    pushResBody.isSuccess = false;
                    notification.State = FailState;
                    db.Entry(notification).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }
                else
                {
                    foreach (var userId in reqBody.userIds)
                    {
                        var message = new ordernotification()
                        {
                            Message = reqBody.message,
                            Title = reqBody.title,
                            CreateTime = DateTime.Now,
                            Tmcode = userId
                        };
                        db.ordernotification.Add(message);
                    }
                    await db.SaveChangesAsync();

                    pushResBody.isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null) db.Dispose();
            }
            if (pushResBody.isSuccess)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
